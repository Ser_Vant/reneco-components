import React from 'react';
import Droppable from './Droppable';
import DroppableHandle from './Handle';

const Component = Droppable(({children}) => (<React.Fragment>{children}</React.Fragment>));
Component.Handle = DroppableHandle;
export default Component;
export const Handle = DroppableHandle;