import React from 'react';
import dragContext, {EVENTS as DRAG_EVENTS, LAST, BASE_CLASS} from './dragContext';
import addClass from 'reneco-utils/addClass';

import './Placeholder.scss';

export default class Placeholder extends React.Component {
    state={
        width: 0,
        height: 0,
        dragStarted: false,
        phWidth: 0,
        phHeight: 0,
        // dragWidth: 0,
        // dragHeight: 0,
        // parentEmpty: false,
        dragHtml: null
    }
    dragWidth = 0;
    dragHeight = 0;
    isParentEmpty(parent) {
        const {parent: cParent, item} = this.props;
        return (parent && parent.getIX()) === (cParent && cParent.getIX())
            ? (cParent.length <= 1 && !item)
            : cParent.length <= 0
    }
    onDragStart = ({item, width, height, exact}) => {
        const {item: cItem, parent} = this.props;
        // console.log(item)
        if((cItem && cItem.getIIX() === item.getIIX() || !cItem) && (!exact || item.parent.getIX() === parent.getIX())) {
            this.setState({
                dragStarted: true,
                // parentEmpty: this.isParentEmpty(parent),
                // dragWidth: width,
                // dragHeight: height
            });
            this.dragWidth = width;
            this.dragHeight = height;
        }

        dragContext.addEventListener(DRAG_EVENTS.over, this.onDragOver);
    }
    onDragEnd = () => {
        this.setState({
            dragStarted: false,
            phWidth: 0,
            phHeight: 0,
            // dragWidth: 0,
            // dragHeight: 0,
            dragHtml: null,
            parentEmpty: false
        });
        dragContext.removeEventListener(DRAG_EVENTS.over, this.onDragOver);
    }
    onDragOver = ({item, over, parent, html}) => {
        const {parent: cParent, item: cItem} = this.props,
            {phHeight, phWidth} = this.state,
            overItem = cItem
                ? cItem.getIIX() === (over && over.getIIX && over.getIIX())
                : over === LAST;
        if(
            !cItem && over === LAST
            ? parent.getIX() === cParent.getIX()
            : overItem
        ) {
            if(phHeight !== item.height || phWidth !== item.width) {
                this.setState({
                    phWidth: item.width,
                    phHeight: item.height,
                    dragHtml: html,
                    // parentEmpty: false
                });
            }
        } else if(phHeight || phWidth) {
            this.setState({
                phWidth: 0,
                phHeight: 0,
                dragHtml: null,
                // parentEmpty: this.isParentEmpty(item.parent)
            });
        }
    }
    componentDidMount() {
        dragContext.addEventListener(DRAG_EVENTS.start, this.onDragStart);
        dragContext.addEventListener(DRAG_EVENTS.end, this.onDragEnd);
    }
    componentWillUnmount() {
        dragContext.removeEventListener(DRAG_EVENTS.start, this.onDragStart);
        dragContext.removeEventListener(DRAG_EVENTS.end, this.onDragEnd);
    }
    render() {
        const {dragHtml, dragWidth, dragHeight, phHeight, phWidth, dragStarted} = this.state,
            baseClass = `${BASE_CLASS}-placeholder`,
            {parent, inline, item} = this.props;
        // console.log(inline)
        return (<div className={addClass(baseClass, inline && `${baseClass}-inline`)}>
            <div
                className={`${baseClass}-item`}
                style={{
                    // width: phWidth,
                    // height: phHeight
                }}
                dangerouslySetInnerHTML={{__html: dragHtml && dragHtml.outerHTML}}
            />
            {!item && dragStarted && <div
                className={`${baseClass}-empty`}
                style={inline
                    ? {
                        // width: this.dragWidth,
                        height: this.dragHeight
                    }
                    : {height: this.dragHeight}}
                onDragOver={e => dragContext.over(e, LAST, parent)}
            />}
        </div>)
    }
}