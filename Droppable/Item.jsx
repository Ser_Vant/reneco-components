import React from 'react';
import ReactDOM from 'react-dom';
import addClass from 'reneco-utils/addClass';
import dragContext, {EVENTS as DRAG_EVENTS, LAST, BASE_CLASS} from './dragContext';
// import Model from './Model';

import './Item.scss';

export default class DroppableItem extends React.Component {
    // model = new Model();
    state={
        width: 0,
        height: 0,
        dragStarted: false,
        isDrag: false
        // hide: false,
    }
    startDrag() {
        setTimeout( () => this.setState({isDrag: true}));
    }
    stopDrag() {
        this.setState({isDrag: false});
    }
    onDragStart = ({item}) => {
        this.setState({
            dragStarted: true,
        });
    }
    onDragEnd = item => {
        this.setState({
            dragStarted: false,
            isDrag: false
            // hide: false
        });
    }
    recalculateSize = () => {
        if(this.$box.offsetHeight !== this.state.height
            || this.$box.offsetWidth !== this.state.width
        ) {
            this.setState({
                width: this.$box.offsetWidth,
                height: this.$box.offsetHeight
            });
        }
    }
    componentDidMount() {
        this.recalculateSize();
        window.document.addEventListener('resize', this.recalculateSize);
        dragContext.addEventListener(DRAG_EVENTS.start, this.onDragStart);
        dragContext.addEventListener(DRAG_EVENTS.end, this.onDragEnd);
    }
    componentWillUnmount() {
        window.document.removeEventListener('resize', this.recalculateSize);
        dragContext.removeEventListener(DRAG_EVENTS.start, this.onDragStart);
        dragContext.removeEventListener(DRAG_EVENTS.end, this.onDragEnd);
    }
    onDragOverItem(e, item) {
        dragContext.over(e, item, this.props.parent);
    }
    getView() {
        return ReactDOM.findDOMNode(this.$children);
    }
    componentDidUpdate() {
        this.recalculateSize();
    }
    render() {
        const {
                item,
                children,
                className,
                next,
                parent,
                inline,
                exact
            } = this.props,
            {
                dragStarted,
                width,
                height,
                isDrag
                // hide,
            } = this.state,
            baseClass = `${BASE_CLASS}-item`,
            overlayWidth = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2)),
            overlayAngle = overlayWidth
                ? (Math.asin(height / overlayWidth))
                : 0,
            overlayHeight = Math.sin(overlayAngle) * width;
        return (<div
            className={addClass(baseClass, inline && `${baseClass}-inline`, isDrag && `${baseClass}-hdd`)}
            ref={ref => this.$wrapper = ref}
        >
            <div
                ref={ref => this.$box = ref}
                className={addClass(className, `${baseClass}-box`)}
                draggable
                onDragStart={e => (view => dragContext.start(e, {
                    parent,
                    item,
                    width: view.offsetWidth,
                    height: view.offsetHeight,
                    exact
                    // html: ReactDOM.findDOMNode(this.$box)
                }, this))(this.getView())}
                onDragEnd={e => dragContext.end(e)}
            >
                <div
                    className={`${baseClass}-children`}
                    ref={ref => this.$children = ref}
                >
                    {children}
                </div>
                {dragStarted && <div className={`${baseClass}-before`}
                    onDragOver={e => this.onDragOverItem(e, item)}
                    style={{
                        width: overlayWidth,
                        height: overlayHeight,
                        transform: `rotate(-${overlayAngle}rad)`
                    }}
                />}
                {dragStarted && <div className={`${baseClass}-after`}
                    onDragOver={e => this.onDragOverItem(e, next || LAST)}
                    style={{
                        width: overlayWidth,
                        height: overlayHeight,
                        transform: `rotate(-${overlayAngle}rad)`
                    }}
                />}
            </div>
        </div>);
    }
}