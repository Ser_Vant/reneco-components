import React from 'react';
import dragContext from './dragContext';
import addClass from 'reneco-utils/addClass';

export default props => {
    const tagName = props.as || 'div';
    return (React.createElement(
        tagName,
        Object.assign({
            className: addClass(props.className, 'droppable-handle'),
            onMouseDown: e => {
                e.stopPropagation();
                dragContext.handlePress(e);
            },
            onMouseLeave: e => {
                e.stopPropagation();
                dragContext.handleLeave(e);
            },
            onMouseUp: e => {
                e.stopPropagation();
                dragContext.handleLeave(e);
            }
        }, Object.keys(props).reduce((prev, cur) => {
            if(['children', 'as', 'className'].indexOf(cur) < 0) {
                prev[cur] = props[cur];
            }
            return prev;
        }, {})),
        props.children
    ))
};