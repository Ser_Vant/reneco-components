import withEvents from 'reneco-utils/withEvents';

const DRAG_EVENTS = {
        start: 'DRAG_START',
        end: 'DRAG_END',
        over: 'DRAG_OVER'
    },
    LAST_VAR = 'last',
    COMPONENT_BASE_NAME = 'reneco-components-droppable',
    DragContext = withEvents(class Context {
        __handlePressed = false;
        __Item = null;
        __parent = null;
        __instance = null;
        __html = null;
        __exact = false;
        handlePress(e) {
            this.__handlePressed = true;
        }
        handleLeave(e) {
            this.__handlePressed = false;
        }
        start(e, item, instance) {
            e.stopPropagation();
            // console.log(this.__handlePressed)
            if(this.__handlePressed) {
                this.__Item = item;
                this.__over = LAST_VAR;
                this.__parent = item.parent;
                this.__instance = instance;
                this.__exact = item.exact;
                // console.log(e.dataTransfer);
                e.dataTransfer.dropEffect = 'move';
                this.__html = (div => {
                    div.classList.add(`${COMPONENT_BASE_NAME}-hidden-element`);
                    div.style.width = item.width;
                    div.style.height = item.height;
                    div.appendChild(this.__instance.getView().cloneNode(true));
                    div.querySelectorAll('[id]').forEach(ch => ch.removeAttribute('id'));
                    document.body.appendChild(div);
                    return div;
                })(document.createElement('DIV'));
                // e.dataTransfer.setDragImage(this.__instance.getView().cloneNode(true), 0, 0);
                e.dataTransfer.setDragImage(this.__html.firstChild, 0, 0);
                this.__instance.startDrag();
                this.dispatchEvent(DRAG_EVENTS.start, this.__Item);
                this.dispatchEvent(DRAG_EVENTS.over, {
                    item: this.__Item,
                    over: this.__over,
                    parent: this.__parent,
                    exact: this.__exact
                });
            } else {
                // console.log('STOP!!!')
                e.preventDefault();
            }
        }
        end(e) {
            e.stopPropagation();
            // this.__instance && this.__instance.stopDrag();
            this.__html && document.body.removeChild(this.__html);
            this.dispatchEvent(DRAG_EVENTS.end, {
                item: this.__Item,
                over: this.__over,
                parent: this.__parent,
                exact: this.__exact
            });
            this.__html = null;
            this.__instance = null;
            this.__Item = null;
            this.__over = null;
            this.__parent = null;
            this.__exact = false;
        }
        over(e, item, parent) {
            e && e.stopPropagation();
            // e && e.preventDefault && e.preventDefault();
            ((cur, prev) => {
                if(cur !== prev || ((parent && parent.getIX()) !== (this.__parent && this.__parent.getIX()))) {
                    if(!this.__exact || parent.getIX() === this.__parent.getIX()) {
                        this.__over = item;
                        this.__parent = parent;
                        this.dispatchEvent(DRAG_EVENTS.over, {
                            item: this.__Item,
                            over: this.__over,
                            parent: this.__parent,
                            html: this.__html.firstChild
                        });
                    }
                }
            })(item === LAST_VAR
                ? item
                : (item && item.getIIX()),
            this.__over === LAST_VAR
                ? this.__over
                : (this.__over && this.__over.getIIX())
            );
        }
        getOver() {
            return this.__over;
        }
        getItem() {
            return this.__Item;
        }
    });

export default new DragContext();
export const EVENTS = DRAG_EVENTS;
export const LAST = LAST_VAR;
export const BASE_CLASS = COMPONENT_BASE_NAME;