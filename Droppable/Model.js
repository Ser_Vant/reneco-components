import ReNode, {EVENTS as RENODE_EVENTS} from 'reneco-utils/ReNode';

let counter = 0;

class ModelItem extends ReNode {
    constructor(props) {
        super(props);
        this.IIX = props.IIX !== undefined ? props.IIX : counter++;
        this.key = props.key;
    }
    getIIX() {
        return this.IIX;
    }
}

export default class Model extends ReNode {
    constructor(props) {
        super(props);
        this._component = props && props.component;
    }
    registerItem(ix, model) {
        console.log(ix, model);
    }
    getNode(ix) {
        return this._component && this._component.getNode(ix);
    }
    getNodeByIX(ix) {
        return this._component && this._component.getNodeByItemIX(ix)
    }
    updateChilds(list, silent) {
        const removeList = this.filterChilds(item => list.findIndex(item2 => item2.IIX === item.IIX) < 0),
            addList = list.filter(item => !this.findChild(item2 => item2.IIX === item.IIX));

        removeList.forEach(item => this.removeChild(item, true));
        addList.forEach(item => this.addChild(
            new ModelItem(item),
            list.findIndex(i => item.IIX !== undefined ? item.IIX === i.IIX : false),
            true
        ));
        this.reorder(list.map(item => this._childs.findIndex(i => i.IIX === item.IIX)), true);
        !silent && this.dispatchEvent(RENODE_EVENTS.change);
        // console.log(list.length, this.length);
    }
}