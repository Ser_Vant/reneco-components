import React from 'react';
import ReactDOM from 'react-dom';
import Poster from '../Poster';
import getDocumentOffset from 'reneco-utils/getDocumentOffset';
import addClass from 'reneco-utils/addClass';
import SlideshowPoster from './Poster';
import SlideshowSlider from './Slider';
import FullscreenComponent from '../Fullscreen';
import Fullscreen from 'reneco-utils/Fullscreen';

import './Slideshow.sass';

export default class FABSlideshow extends React.Component{
    static defaultProps = {
        list: [],
        autoplay: 3,
        showSlider: true
    }
    constructor(props) {
        super(props);
        this.state = {
            fullscreen: props.fullscreen === true,
            selected: props.selected || 0,
            showSlider: props.showSlider,
            height: props.height || 'auto',
            width: props.width || 'auto',
            isPlay: false
        };
        this.playTO = null;
    }
    onPlay(){
        if(!this.state.isPlay) {
            this.setState({isPlay: true});
        }
        this.playTO && clearTimeout(this.playTO);
        this.playTO = setTimeout(()=>{
            this.onNext();
            this.onPlay();
        }, this.props.autoplay * 1000);
    }
    onPause() {
        if (this.state.isPlay) {
            this.setState({ isPlay: false });
        }
        this.playTO && clearTimeout(this.playTO);
    }
    onTogglePlay = e => {
        if(this.state.isPlay) {
            this.onPause();
        } else {
            this.onPlay();
        }
    }
    onFullScreenToggle = e => {
        if(this.state.fullscreen) {
            this.$Fullscreen.exitFullscreen();
        } else {
            this.$Fullscreen.fullscreen();
        }
    }
    onExitFullscreen = e => {
        this.setState({
            fullscreen: false
        });
    }
    componentWillUnmount() {
        this.playTO && clearTimeout(this.playTO);
    }
    onSelect = (ix, e) => {
        if(this.state.selected !== ix) {
            this.setState({
                selected: ix
            });
            if(ix >= this.props.list.length-1) {
                this.props.onLastItem && this.props.onLastItem(this.props.list[this.props.list.length - 1]);
            }
        }
    }
    onToggleSlideshow = e => {
        this.setState({
            showSlider: !this.state.showSlider
        });
    }
    onPrev = e => {
        let newIx = this.state.selected - 1;
        newIx = newIx < 0 ? this.props.list.length - 1 : newIx;
        this.onSelect(newIx);
    }
    onNext = e => {
        let newIx = this.state.selected + 1;
        newIx = newIx > this.props.list.length - 1 ? 0 : newIx;
        this.onSelect(newIx);
    }
    render() {
        let props = this.props,
            state = this.state,
            baseClass = 'fab-slideshow',
            imgClassName = baseClass + '-image-box',
            selectedItem = props.list[state.selected],
            width = props.width || 'auto',
            height = props.height || 'auto',
            imgBoxHeight = state.showSlider && props.height && Math.ceil(props.height * 0.9) || props.height;
        return (<FullscreenComponent
            fullscreen={props.fullscreen}
            onExit={this.onExitFullscreen}
            ref={ref=>{ this.$Fullscreen = ref; }}
            className={baseClass}
            style={{
                width: width,
                height: height
            }}
        >
            <div className={imgClassName}>
                <div style={{ height: imgBoxHeight }} className={imgClassName+'-list'}>
                    <div
                        style={{marginLeft: state.selected * width * -1}}
                        className={imgClassName + '-list-slider'}
                    >
                        {props.list.map((item, ix) => {
                            return (<div
                                key={ix}
                                className={imgClassName + '-list-img'}
                                style={{width: width}}
                            >
                                <SlideshowPoster
                                    height={imgBoxHeight}
                                    imgWidth={item.width}
                                    imgHeight={item.height}
                                    src={item.image}
                                    alt={item.title}
                                    waitImage={[state.selected - 1, state.selected, state.selected + 1].indexOf(ix) < 0}
                                />
                            </div>);
                        })}
                    </div>
                    {/*<div className={imgClassName+'-description'}>
                        <div className={imgClassName + '-description-content'}>
                            {selectedItem.description}
                        </div>
                        <button className={imgClassName + '-description-button'} />
                    </div>*/}
                </div>
                <button className={imgClassName + '-button-prev'} onClick={this.onPrev}>
                    <div className={imgClassName + '-button-prev-icon'} />
                </button>
                <button className={imgClassName + '-button-next'} onClick={this.onNext}>
                    <div className={imgClassName + '-button-next-icon'} />
                </button>
                <div className={imgClassName + '-buttons'}>
                    <button
                        className={
                            state.isPlay
                                ? imgClassName + '-buttons-pause'
                                : imgClassName + '-buttons-play'
                        }
                        onClick={this.onTogglePlay}
                    />
                    {Fullscreen.isEnabled() && <button
                        onClick={this.onFullScreenToggle}
                        className={
                            state.fullscreen
                                ? (imgClassName + '-buttons-normalscreen')
                                :(imgClassName + '-buttons-fullscreen')
                        }
                    />}
                </div>
            </div>
            <SlideshowSlider
                list={props.list}
                selected={state.selected}
                onSelect={this.onSelect}
                onToggle={this.onToggleSlideshow}
                onNext={this.onNext}
                onPrev={this.onPrev}
                height={height - imgBoxHeight}
                show={state.showSlider}
            />
        </FullscreenComponent>);
    }
}
