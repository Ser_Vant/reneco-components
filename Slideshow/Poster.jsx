import React from 'react';
import Loader from '../Loader';
import Poster from '../Poster';

import './Poster.sass';

export default class SlideshowPoster extends React.Component{
    static defaultProps = {
        imgWidth: 0,
        imgHeight: 0
    }
    constructor(props) {
        super(props);
        this.state = {
            titleHeight: 0,
            boxWidth: 0
        };
    }
    componentDidMount() {
        this.calculateTitleHeight();
    }
    componentDidUpdate() {
        this.calculateTitleHeight();
    }
    calculateTitleHeight() {
        let h = this.$Text && this.$Text.offsetHeight || 0,
            boxWidth = this.$Box.offsetWidth;
        if(
            h !== this.state.titleHeight
            || boxWidth !== this.state.boxWidth
        ) {
            this.setState({
                titleHeight: h,
                boxWidth: boxWidth
            });
        }
    }
    render() {
        let props = this.props,
            state = this.state,
            width = (props.imgWidth / props.imgHeight * (props.height - state.titleHeight)) || 0,
            baseClass = 'fab-slideshow-poster';
        if (state.boxWidth && width > state.boxWidth) {
            width = state.boxWidth;
        }
        return (<div ref={ref=>{this.$Box = ref;}} className={baseClass}>
            <div style={{
                width: width || 'auto'
            }} className={baseClass+'-sizer'}>
                <Poster
                    src={props.src}
                    alt={props.alt}
                    title={props.alt}
                    waitImage={props.waitImage}
                />
            </div>
            {props.alt && <div
                ref={ref=>{ this.$Text = ref; }}
                className={baseClass + '-title'}
            >{props.alt}</div>}
        </div>);
    }
}
