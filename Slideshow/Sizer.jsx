import React from 'react';
import getDocumentOffset from 'reneco-utils/getDocumentOffset';

export default class FABSlideshowSizer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            width: null,
            height: null
        };
    }
    componentDidMount() {
        window.addEventListener('resize', this.onResize);
        this.onResize();
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.onResize);
    }
    onResize = e => {
        let state = this.state,
            width = this.Box.offsetWidth,
            height = getDocumentOffset().height;
        if(width !== state.width || height !== state.height) {
            this.setState({
                width: width,
                height: height
            });
        }
    }
    render() {
        let state = this.state,
            props = this.props;
        return (<div ref={ref=>{ this.Box = ref; }}>
            {state.width && state.height && React.cloneElement(props.children, {
                width: state.width,
                height: state.height - 175
            })}
        </div>);
    }
}
