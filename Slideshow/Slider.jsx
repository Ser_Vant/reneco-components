import React from 'react';
import Poster from '../Poster';
import addClass from 'reneco-utils/addClass';
import TouchScroll from '../Slider/TouchScroll';
import './Slider.sass';

export default class SlideshowSlider extends React.Component {
    static defaultProps = {
        show: true
    }
    constructor(props) {
        super(props);
        this.state = {
            marginLeft: 0
        };
        this.widths = [];
    }
    onSelect = ix => {
        this.props.onSelect && this.props.onSelect(ix);
    }
    onToggle = e => {
        this.props.onToggle && this.props.onToggle();
    }
    onPrev = e => {
        this.props.onPrev && this.props.onPrev(e);
    }
    onNext = e => {
        this.props.onNext && this.props.onNext(e);
    }
    componentWillReceiveProps(nProps) {
        if(nProps.selected !== this.props.selected) {
            this.$touchScroll.select(nProps.selected); 
        }
    }
    render() {
        let props = this.props,
            state = this.state,
            baseClass = 'fab-slideshow-slider';
        this.widths = [];

        return (<div
            className={baseClass}
        >
            <TouchScroll 
                ref={ref => {this.$touchScroll = ref;}}
                className={baseClass+'-touch-scroll'} 
                style={{
                    height: props.show && props.height || 0
                }} 
            >
                    {props.list.map((item, ix) => {
                        let itemClass = baseClass + '-box-item',
                            previewWidth = item.width / item.height * (props.height);
                        this.widths[ix] = previewWidth + 10; // plus left and right margin
                        return (<div
                            key={ix}
                            className={addClass(itemClass, ix === props.selected && (itemClass + '-selected'))}
                            style={{ width: previewWidth }}
                        >
                            <Poster
                                className={itemClass+'-img'}
                                src={item.image}
                                alt={item.title}
                                title={item.title}
                            />
                            <div className={itemClass + '-overlay'} onClick={this.onSelect.bind(null, ix)} />
                        </div>);
                    })}
            </TouchScroll>
            <button
                className={addClass(
                    baseClass + '-button-toggle',
                    props.show
                        && baseClass +'-button-toggle-show'
                        || (baseClass + '-button-toggle-hide' )
                )}
                onClick={this.onToggle}
            />
        </div>);
    }
}
