import React from 'react';

import Popup from '../Popup';
import getDocumentOffset from 'reneco-utils/getDocumentOffset';

import Slideshow from './Slideshow';
import Sizer from './Sizer';

import './SlideshowPopup.sass';

let counter = 0;

export default class SlideshowPopup extends React.Component {
    galleria = null
    ix = null
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            fullyloaded: false,
            width: 0,
            height: 0,
            delay: props.delay,
            list: this.prepareList(props.list)
        };
        this.ix = 'gallery-' + counter++;
    }
    prepareList(list) {
        return (list || []).map(this.getItem);
    }
    getItem = item => {
        return  this.props.getItem && this.props.getItem(item) || item;
    }
    // resizeWindow = () => {
    //     let size = getDocumentOffset(),
    //         width = size.width,
    //         height = size.height;
    //     console.log(width, height, this.Box);
    //     if (
    //         height !== this.state.height
    //         || width !== this.state.width
    //     ) {
    //         this.setState({
    //             width: width,
    //             height: height
    //         });
    //     }
    // }
    onLastItem = g => {
        if (!this.state.fullyloaded && !this.state.loading) {
            this.props.onLast && this.props.onLast(this);
        }
    }
    startLoad() {
        this.setState({ loading: true });
    }
    getItemIndex(item) {
        let preparedItem = this.getItem(item);
        return this.state.list.findIndex(item => {
            return item.image === preparedItem.image;
        });
    }
    render() {
        let props = this.props,
            state = this.state,
            selected = props.showItem && this.getItemIndex(props.showItem);

        selected = selected > -1 ? selected : null;
        return (<Popup
            name='slideshow'
            className='fab-slideshow-popup'
            wide
            useOverlay={true}
            onHide={props.onHide}>
            <div>
                <Sizer>
                    <Slideshow
                        autoplay={state.delay}
                        list={state.list}
                        selected={selected}
                        onLastItem={this.onLastItem}
                        ref={ref => { this.Sshow = ref; }}
                        className='slideshow-box'
                    />
                </Sizer>
            </div>
        </Popup>);
    }
}
