import React from 'react';
import './Text.sass';
import addClass from 'reneco-utils/addClass';

export default function TrixText(props) {
    let pr = {
        style: props.style || {},
        className: addClass('trix-text', props.className)
    };
    return (props.text && <div
        dangerouslySetInnerHTML={{ __html: props.text }}
        {...pr}
    /> || <div {...pr} >{props.children}</div>);
}
