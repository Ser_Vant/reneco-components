import React from 'react';
import Text from './Text';
import Slideshow from '../Slideshow';

import './WithoutImages.sass';

export default class WithoutImages extends React.Component {
    constructor(props) {
        super(props);
        this.images = {};
        this.imgOrdered = [];
        this.state = {
            imgList: [],
            selected: null
        };
    }
    componentDidMount() {
        this.arrangeImages();
    }
    componentDidUpdate() {
        this.arrangeImages();
    }
    arrangeImages() {
        (this.Box && this.Box.querySelectorAll('figure.attachment') || []).forEach(item => {
            let image = item.querySelector('img'),
                src = image.src;
            if(src) {
                if (!this.images[src]) {
                    this.images[src] = {
                        $: item,
                        onClick: this.onImageClick.bind(null, src),
                        img: {
                            image: src.replace('medium', 'large'),
                            width: image.offsetWidth,
                            height: image.offsetHeight,
                            description: '',
                            title: ''
                        }
                    };
                    this.images[src].$.addEventListener('click', this.images[src].onClick);
                    this.imgOrdered.push(src);
                }
            }
        });
    }
    onImageClick = src => {
        this.setState({
            selected: this.images[src].img,
            imgList: this.imgOrdered.map(item => {
                return this.images[item].img;
            })
        });
    }
    onHide = () => {
        this.setState({
            imgList: [],
            selected: null
        });
    }
    componentWillUnmount() {
        Object.keys(this.images).map(src=>{
            let item = this.images[src];
            item.$.removeEventListener('click', item.onClick);
            item.img = null;
        });
        this.images = null;
        this.imgOrdered = null;
    }
    render() {
        let props = this.props,
            state = this.state,
            baseClass = 'trix-text-without-images';
        return (<div ref={ref => { this.Box = ref; }} className={baseClass}>
            <Text className={baseClass+'-text'} {...props} />
            {state.imgList
                && state.imgList.length>0
                && <Slideshow onHide={this.onHide} list={state.imgList} showItem={state.selected} />}
        </div>);
    }
}
