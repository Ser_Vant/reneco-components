import React from 'react';
import CustomTag from '../CustomTag';
import addClass from 'reneco-utils/addClass';
import isFoo from 'reneco-utils/isFoo';

export default class Menu extends React.Component{
    state = {
        selected: -1
    }
    _sub = {}
    $items = {}
    setSelected(newValue){
        if(newValue !== this.state.selected) {
            this.setState({selected: newValue});
            this.props.onSet && this.props.onSet(newValue);
            Object.keys(this._sub).map(key => {
                const item = this._sub[key];
                if(item.ix !== newValue) {
                    item.submenu.setSelected(-1);
                }
            });
        }
    }
    getSelected(){
        let selected = this.props.list[this.state.selected];
        if (selected && selected.childs && selected.childs.length>0
            && this._sub[this.state.selected]) {
            return this._sub[this.state.selected].submenu.getSelected();
        } else {
            return this.props.list[this.state.selected]
                && this.props.list[this.state.selected].to;
        }
    }
    selectTo(to){
        let ix = (this.props.list||[]).findIndex(item=>{
            return item.to === to;
        });

        if(ix>-1) {
            this.setSelected(ix);
        } else {
            Object.keys(this._sub).map(item => this._sub[item].submenu.selectTo(to));
        }
    }
    getFiltered(){
        return (this.props.list || []).filter(item=>{
            return item.caption !== undefined
                && item.caption !== false
                && item.caption !== null
                && item.caption !== '';
        });
    }
    onSelect(selectedId, e){
        let filtered = this.getFiltered(),
            selectedItem = filtered[selectedId],
            itemDest = selectedItem && filtered[selectedId].to;
        if(this.props.onSelect || isFoo(itemDest)) {

            e && e.preventDefault();
            this.setSelected(selectedId);
            if (!selectedItem.childs || selectedItem.childs.length===0) {
                if(isFoo(itemDest)) {
                    itemDest(selectedId, e);
                } else {
                    this.props.onSelect && this.props.onSelect(itemDest, this.$items[selectedId], selectedId);
                }
            } else if (this._sub[selectedId]){
                this._sub[selectedId].submenu.onSelect(0, e);
            }
        }
    }
    onChildSelected(ix, subIx){
        this.setSelected(ix);
        this.props.onSelect(subIx);
    }
    componentDidMount(){
        if (this.props.selected) {
            this.selectTo(this.props.selected);
        }
    }
    render() {
        let props = this.props;
        return (<ul
            className={addClass('menu', props.className)}
            style={props.style}
        >
            {this.getFiltered().map((item, ix)=>{
                let isCallback = isFoo(item.to);
                return (<li
                    ref={ref => { this.$items[ix] = ref; }}
                    key={(!isCallback && item.to || '')+item.caption}
                    className={addClass(
                        addClass(
                            'menu-item',
                            ix === this.state.selected
                                ?((props.className||'')+'__item--current item--current')
                                :''
                        ),
                        (props.className||'')+'__item'
                    )}
                >
                    {(props.anchorNavigation || isCallback || props.onSelect)
                        && <CustomTag
                            tag={props.itemTag || 'a'}
                            onClick={isCallback ? item.to : this.onSelect.bind(this, ix)}
                            href={(!isCallback && item.to)||item.url||'javascript:void(0)'}>{
                                (item.caption || '')
                            }</CustomTag>
                        || <CustomTag tag={props.itemTag} to={item.to}>{(item.caption || '')}</CustomTag>}
                    {item.childs && item.childs.length>0 && <Menu
                        anchorNavigation={props.anchorNavigation}
                        onSet={this.onChildSelected.bind(this, ix)}
                        onSelect={this.props.onSelect}
                        ref={ref=>{this._sub[ix]={ix: ix, submenu: ref};}}
                        list={item.childs}
                    />}
                </li>);
            })}
        </ul>);
    }
}
