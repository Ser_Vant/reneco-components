//импорт глобальных статических бибилитек
import React from 'react';
import {createRoot} from 'react-dom/client';
import Application, {Route} from './Demo';

import Scheme from './Scheme/Demo';

const container = document.getElementById('body'),
    root = createRoot(container);

//импорт собранных стиле
// import 'site/colected.scss';
// import 'generated/include';



root.render(<Application>
    <Scheme />
</Application>);