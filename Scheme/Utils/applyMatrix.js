export default (x, y, matrix) => ({
    x: x*matrix[0][0] + y*matrix[1][0] + matrix[2][0],
    y: x*matrix[0][1] + y*matrix[1][1] + matrix[2][1]
});