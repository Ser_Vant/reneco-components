import getMatrixRotation from './getMatrixRotation';
import applyMatrix from './applyMatrix';

export default (x, y, endSize, angle) => {
    const matrix = getMatrixRotation(angle),
        points = [
            {x: -endSize/2, y: 0},
            {x: -endSize/2+endSize/3, y: 0},
            {x: -endSize/2, y: -endSize/2},
            {x: endSize/2, y: 0},
            {x: -endSize/2, y: endSize/2},
            {x: -endSize/2+endSize/3, y: 0},
            {x: -endSize/2, y: 0},
        ]
            .map(p => applyMatrix(p.x, p.y, matrix));
    return `M${points[0].x + x},${points[0].y + y}`
        + ` ${points.slice(1).map(p => `L${p.x + x},${p.y + y}`).join(' ')}`;
}