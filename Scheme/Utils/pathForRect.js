export default (rect, rTop, rBottom) => {
    let path = '';
    const x = rect.x || 0,
        y = rect.y || 0,
        width = rect.width && isFinite(rect.width) ? rect.width : 0,
        height = rect.height && isFinite(rect.height) ? rect.height : 0;
    // console.log(rect)
    if (rTop) {
        path += `M${x},${y + rTop} A${rTop},${rTop} 0 0 1 ${x + rTop},${y}`;
        path += `H${x + width - rTop} A${rTop},${rTop} 0 0 1 ${x + width},${y + rTop}`;
    } else {
        path += `M${x},${y} H${x + width}`;
    }

    if (rBottom) {
        path += `V${y + height - rBottom} A${rBottom},${rBottom} 0 0 1 ${x + width - rBottom},${y + height}`;
        path += `H${x + rBottom} A${rBottom},${rBottom} 0 0 1 ${x},${y + height - rBottom}`;
    } else {
        path += `V${y + height} H${x}`;
    }

    path += 'Z';

    return path;
}