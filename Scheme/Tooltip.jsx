import React, { useCallback, useEffect, useRef, useState } from 'react';
import pathForRect from './Utils/pathForRect';

export default ({
    right,
    blockWidth,
    fontStyle,
    fontWeight = 'bold',
    fontSize,
    fontFamily,
    paddingHorizontal,
    paddingVertical,
    topRadius = 0,
    bottomRadius = 0,
    borderColor,
    text
}) => {
    const $text = useRef(null),
        [[w, h], setTooltipWidth] = useState([0,0]),
        resize = useCallback(() => (tBox => setTooltipWidth([tBox?.width || 0, tBox?.height || 0]))($text.current.getBBox()), []);
    useEffect(() => {
        resize();
    }, [text]);
    return (({width: tWidth, height: tHeight}) => (<g
        transform={`translate(${blockWidth-tWidth-right}, ${-tHeight})`}
    >
        <path
            d={pathForRect({
                x: 0,
                y: 0,
                width: tWidth,
                height: tHeight
            }, topRadius, bottomRadius)}
            fill={borderColor}
        />
        <text
            ref={$text}
            x={tWidth/2}
            y={(tHeight-fontSize)}
            style={{
                dominantBaseline: 'hanging',
                textAnchor: 'middle'
            }}
            fontFamily={fontFamily}
            fill={'#fff'}
            fontSize={fontSize}
            fontStyle={fontStyle}
            // textDecoration={cDecoration || ''}
            fontWeight={fontWeight}
        >
            {text}
        </text>
    </g>))({
        width: w + 2 * paddingHorizontal,
        height: h + paddingVertical * ((topRadius ? 1 : 0) + (bottomRadius ? 1 : 0)),
    })
}