import { useCallback, useMemo, useRef } from 'react';
import useDrag from './useDrag';

export default (position, borderWidth, {
    onMove: _onMove,
    onMoveEnd,
    onMoveStart,  
    onSelect
}, ref) => {
    const fix = borderWidth%2 ? .5 : 1, 
        getTranslation = useCallback(([x,y], point = '') => `translate(${Math.floor(x || 0)+ fix}${point}, ${Math.floor(y || 0)+fix}${point})`, []),
        _$box = useRef(null),
        $box = ref || _$box,
        onMove = position => {
            $box.current.style.transform = getTranslation(position, 'px');
            _onMove && _onMove(position);
        },
        [, x, y] = useDrag(position, null, {onMove, onMoveEnd, onMoveStart, onSelect}, $box),
        transformString = useMemo(() => getTranslation([x,y]), [x, y])
    return [$box, transformString, [x,y]];
}