import useRows from './useRows';
export default onChange => {
    const [, register, unregister, update, getRows] = useRows(onChange);
    return [getRows, register, unregister, update];
}