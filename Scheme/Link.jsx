import React from 'react';
import getArrowPath from './Utils/getArrowPath';
// import getMiddlePoint from 'reneco-utils/Curves/getMiddlePoint';
// import IconDelete from './icons/close.svg';

import useLink from './useLink';

import './Link.scss';
export default ({
        // selected,
        onClick,
        // onClickDelete,
        x1,
        y1,
        x2,
        y2,
        targetPosition,
        endSize = 8,
        width = 2,
        color,
        // buttonSize = 15
    }) => {
        const baseClass = 'reneco-components-scheme-link',
            [c1x, c1y, c2x, c2y] = useLink({x1, x2, y1, y2, targetPosition}),
            p = `M${x1},${y1} C${c1x},${c1y} ${c2x},${c2y} `,
            p1 = p
                + (targetPosition === 'left'
                    ? (x2 - endSize)
                    : targetPosition === 'right'
                        ? (x2 + endSize)
                        : x2
                )
                + ','
                + (targetPosition === 'top'
                    ? (y2 - endSize)
                    : targetPosition === 'bottom'
                        ? (y2 + endSize)
                        : y2
                );
        return (<>
            <path
                className={baseClass}
                d = {p1}
                strokeWidth={width}
                stroke={color}
            />
            <path
                // className={baseClass}
                d = {getArrowPath(
                    targetPosition === 'left'
                        ? (x2 - endSize / 2)
                        : targetPosition === 'right'
                            ? (x2 + endSize / 2)
                            : x2,
                    targetPosition === 'top'
                        ? (y2 - endSize / 2)
                        : targetPosition === 'bottom'
                            ? (y2 + endSize / 2)
                            : y2,
                    endSize,
                    targetPosition === 'top'
                        ? Math.PI / 2
                        : (targetPosition === 'right'
                            ? Math.PI
                            : (targetPosition === 'bottom'
                                ? Math.PI * 3/2
                                : Math.PI * 2
                        )
                    )
                )}
                fill={color}
                strokeWidth={width}
                stroke={color}
            />
            <path
                className={`${baseClass}-handle`}
                d = {`${p}${x2},${y2}`}
                strokeWidth={endSize}
                onClick={onClick}
            />
            {/* {selected && onClickDelete && <>
                <circle
                    cx={deletePoint[0]}
                    cy={deletePoint[1]}
                    r={buttonSize}
                    fill={'#ffffff'}
                    stroke={color}
                />
                {<image
                    x={deletePoint[0] - buttonSize/2}
                    y={deletePoint[1] - buttonSize/2}
                    xlinkHref={IconDelete}
                    width={buttonSize}
                    height={buttonSize}
                />}

                <circle
                    className={`${baseClass}-handle`}
                    cx={deletePoint[0]}
                    cy={deletePoint[1]}
                    r={buttonSize + buttonSize*0.3}
                    onClick={onClickDelete}
                />
            </>} */}
        </>)
    };



// export default Link;