import { useMemo } from 'react';
export default ({x1, x2, y1, y2, targetPosition}) => {
    const pathPadding = useMemo(() => Math.min((Math.sqrt(Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2))) * .5, 70), [x1, x2, y1, y2]),
        c1x = x1 + pathPadding,
        c1y = y1,
        c2x = useMemo(() => targetPosition === 'left'
            ? (x2 - pathPadding)
            : targetPosition === 'right'
                ? (x2 + pathPadding)
                : x2, [x2, targetPosition, pathPadding])
        ,
        c2y = useMemo(() => targetPosition === 'top'
            ? (y2 - pathPadding)
            : targetPosition === 'bottom'
                ? (y2 + pathPadding)
                : (y2), [y2, targetPosition, pathPadding]);
    return [c1x, c1y, c2x, c2y];
}