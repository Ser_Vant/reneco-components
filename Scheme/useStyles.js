import { useRef, useMemo, useCallback } from 'react';
import defaultStyle from './defaultStyle';

export default styles => {
    const stylesBase = useMemo(() => ({...defaultStyle, ...styles}), [styles]),
        cached = useRef({}),
        clear = useCallback(() => Object.keys(cached.current).forEach(k => {
            delete cached.current[k];
        }), []),
        getStyleValue = useCallback((style, delimiter) => {
            if(stylesBase.hasOwnProperty(style)) {
                return stylesBase[style];
            } else {
                const splitted = style.split(delimiter).reverse();
                for(let i=0; i < splitted.length; i++) {
                    const ka = splitted.slice(1+i),
                        k = ka.reverse().concat([splitted[0]]).join(delimiter);
                    if(stylesBase.hasOwnProperty(k)) {
                        return stylesBase[k];
                    } else { /// А если присутствует hover
                        let iix;
                        const ka2 = ka.slice();
                        while((iix = ka2.findIndex(kk => ['hover', 'selected'].indexOf(kk) > -1)) > -1) {
                            ka2.splice(iix, 1);
                            const k2 = ka2.concat([splitted[0]]).join(delimiter);
                            if(stylesBase.hasOwnProperty(k2)) {
                                return stylesBase[k2];
                            }
                        }
                    }
                }
                return false;
            }            
        }, [styles]),
        getStyle = useCallback((style, isBG) => {
            if(!cached.current.hasOwnProperty(style)) {
                cached.current[style] = (s => (isBG && !Array.isArray(s)) ? [s] : s)(getStyleValue(style, '.'));
            }
            return cached.current[style];
        }, [styles]);
    useMemo(clear, [styles]); // useMemo сразу отрабатывает, а useEffect  отрабатывает после рендера. Поэтому так
    return [getStyle, clear];
}