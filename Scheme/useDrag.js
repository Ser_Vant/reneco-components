import { useState, useRef, useCallback, useEffect, useContext, useMemo } from 'react';
import {Context as SchemeContext} from './SchemeContext';
import {context} from './useDrop';

export default (position, type, {
    svg: pSvg,
    zoom: pZoom,
    context: cx,
    resetPosition,
    onMove,
    onMoveEnd,
    onMoveStart,  
    reverse,
    onSelect: onSelectBlocks  
}, ref) => {
    const [[x, y], setPosition] = useState(position || []),
        cxKey = (cx || []).join('-'),
        cp = useRef([x, y]),
        {svg: cSvg, zoom: cZoom} = useContext(SchemeContext) || {},
        svg = pSvg || cSvg,
        zoom = pZoom || cZoom,
        _$box = useRef(null),
        $box = ref || _$box,
        coords = useRef([0,0,0,0]),
        eMouseMove = 'mousemove',
        eMouseUp = 'mouseup',
        eMouseLeave = 'mouseleave',
        eMouseOver = 'mouseover',
        unlisten = useRef([]),
        onDrag = useCallback(e => {
            cp.current = [
                coords.current[2] + ((zoom || 1) * (e.pageX - coords.current[0]) * (reverse ? -1 : 1)),
                coords.current[3] + ((zoom || 1) * (e.pageY - coords.current[1]) * (reverse ? -1 : 1))
            ];
            onMove && onMove(cp.current);
        }, [zoom, cxKey]),
        onMouseUp = useCallback(e => {
            if(resetPosition)  {
                cp.current = coords.current.slice(-2);
            }
            setPosition(cp.current);
            onMoveEnd && onMoveEnd(cp.current);
            unlisten.current.forEach(u => u());
            unlisten.current = [];
        }, [zoom, cxKey]),    
        onDragOver = useCallback((e, ix) => {
            if(!context.over || context.over[0] !== type || context.over[1] !== ix) {
                context.over = [type, ix];
                const b = context.blocks.find(([iix]) => iix === ix);
                if(b) { b[3].onDragOver && b[3].onDragOver(e, ix);}
            }
        }, []),   
        onDrogOverOut = useCallback((e, ix) => {
            const b = context.blocks.find(([iix]) => iix === ix);
            if(b) { b[3].onDragOut && b[3].onDragOut(e, ix);}
            context.over = null;
        }, []), 
        onMouseDown = useCallback(e => {
            e.stopPropagation();
            coords.current = [e.pageX, e.pageY, cp.current[0], cp.current[1]];
            svg.current.addEventListener(eMouseMove, onDrag);
            svg.current.addEventListener(eMouseLeave, onMouseUp);
            svg.current.addEventListener(eMouseUp, onMouseUp);
            unlisten.current.push(() => {
                svg.current.removeEventListener(eMouseMove, onDrag);
                svg.current.removeEventListener(eMouseLeave, onMouseUp);
                svg.current.removeEventListener(eMouseUp, onMouseUp);    
                    context.drag = null;
                coords.current = [0,0,0,0];                           
            });
            context.blocks.filter(([,t]) => t === type).forEach(([ix,,$b]) => {
                const _onDragOver = e => onDragOver(e, ix),
                    _onMouseLeave = e => onDrogOverOut(e, ix);
                $b.current.addEventListener(eMouseOver, _onDragOver);
                $b.current.addEventListener(eMouseLeave, _onMouseLeave);
                unlisten.current.push(() => {
                    $b.current.removeEventListener(eMouseOver, _onDragOver);
                    $b.current.removeEventListener(eMouseLeave, _onMouseLeave);                    
                })
            });
            context.drag = cx;
            onMoveStart && onMoveStart(cp.current);
            onSelectBlocks && onSelectBlocks(cp.current);
        }, [zoom, cxKey]);
    useEffect(() => {
        if(x !== position[0] || y !== position[1]) {
            cp.current = position;
            setPosition(position);
        }
    }, [position[0], position[1]])
    useEffect(() => (eMouseDown => {
        $box.current?.addEventListener(eMouseDown, onMouseDown);
        return () => {
            $box.current?.removeEventListener(eMouseDown, onMouseDown);
        }
    })('mousedown'), [cxKey, zoom]);   
    return [$box, x, y];
}