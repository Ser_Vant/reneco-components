export default {
    //Рамки
    'topRadius': 5,
    'bottomRadius': 5,
    'borderWidth': 1,
    'borderColor': '#7a97c0',

    //отступы
    'paddingHorizontal': 2,  //8
    'paddingVertical': 2,

    //'background': ['#DAE8FC','#FFFFFF'],
    'background': ['#e8f2ff'],



    'fontFamily': 'Helvetica, Arial',
    'fontSize': 14,

    'fontWeight': 'bold',
    //'fontStyle': 'italic',


    //'dropShadow': '0px 0px 10px #a0a0a0',
    //    'dropShadow': '1px 1px 2px #BDBDBD',

    'textColor': '#000000',




    //'textAlign': 'right',
    //'textDecoration': 'underline',

    'header.background': ['transparent'],
    'header.borderWidth': 0,
    //'header.borderColor': "#FF0000",
    'header.fontWeight': 'bold',
    //'header.fontStyle': 'italic',
    'header.textColor':'#4A4A4A',
    'header.textAlign': 'left',

    'row.textAlign': 'right',
    'row.textColor':'#959595',

    'row.connector.radius': 4,
    'row.connector.borderColor': '#6C8EBF',
    //'row.connector.borderWidth': 1,
    'row.connector.background': ['#FFFFFF'],
    // 'row.connector.dropShadow': '3px 3px 3px #FF0000',


    //'row.hover.connector.borderColor': '#FF0000',
    'row.hover.connector.borderColor': '#FF0000',
    'row.hover.connector.dropShadow': '0px 0px 3px #000000',

    'hover.dropShadow': '0px 0px 2px #78bbff',
    //'hover.borderColor': '#5e91dd',
    // 'hover.header.textColor': 'blue',
    //'hover.header.textColor': '#6C8EBF',
    // 'hover.header.background': ['transparent'],
    //'hover.header.borderWidth': 0,

    // 'selected.background': ['red'],
    // 'selected.hover.background': ['yellow'],

    'selected.dropShadow': '0px 0px 2px #064B93',
    'selected.hover.dropShadow': '0px 0px 2px #064B93',



    'link.color': '#6C8EBF',
    'link.width': 1,
    'icon': '',
    'iconSize': 16,


    'header.counter.paddingVertical': 1,
    'header.counter.paddingHorizontal': 2,
    'header.counter.rightMargin': 6,
    'header.counter.align': 'center',
    // 'header.counter.textColor':
    // 'header.counter.textDecoration':
    // 'header.counter.fontFamily':
    // 'header.counter.fontSize': 13,
    // 'header.counter.textWeight': 'none'
    // 'header.counter.fontStyle':
};