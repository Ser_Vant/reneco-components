import { useEffect, useId, useRef } from 'react';

export const context = {
    blocks: [],
    drag: null
};
export default (_ix, type, ref, {
    onDragOver,
    onDragOut
}) => {
    const _$box = useRef(null),
        ix = _ix || useId(),
        $box = ref || _$box;
    useEffect(() => {
        context.blocks.push([ix, type, $box, {onDragOver, onDragOut}]);
        return () => {
            const fix = context.blocks.findIndex(([iix]) => iix === ix);
            if(fix > -1) {
                context.blocks.splice(fix, 1);
            }
        }
    }, [_ix, type, ref?.current]);
    return [];
}