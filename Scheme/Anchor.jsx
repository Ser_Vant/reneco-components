import React, { useEffect, useId } from 'react'
import Gradient from './Gradient';
import DropShadow from './DropShadow';
import useDrag from './useDrag';

export default ({
    className,
    x,
    y,
    radius,

    background,
    borderWidth,
    borderColor,
    dropShadow,
    onMove,
    onMoveEnd,
    onMoveStart,  

    context
}) => {
    const ix = useId(),
        bgID = `${ix}-bg`,
        shadowID = `${ix}-shadow`,
        // onSelect = console.log,
        [$box] = useDrag([x, y], 'block', {
            resetPosition: true,
            context,
            onMove,
            onMoveEnd,
            onMoveStart,  
            // onSelect            
        });
    return (<g ref={$box}>
        <defs>
            {background.length > 1 && <Gradient colors={background} id={bgID} />}
            {dropShadow && <DropShadow dropShadow={dropShadow} id={shadowID} />}
        </defs>
        <circle
            stroke={borderColor}
            fill={background.length > 1 ? `url(#${bgID})`: background[0]}
            strokeWidth={borderWidth}
            className={className}
            cx={x}
            cy={y}
            r={radius || 0}
            // onMouseDown={}
            // onMouseUp={e => this.onMouseUp(e)}
            // onMouseOver={e => onMouseOver && onMouseOver(e)}
            // onMouseLeave={e => onMouseLeave && onMouseLeave(e)}
            filter={dropShadow && `url(#${shadowID})` || ''}
        />                
    </g>)
}