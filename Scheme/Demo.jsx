import React from 'react';

import Scheme, {
    Block as SchemeBlock,
    BlockRow as SchemeBlockRow,
    RowFragment
} from './Scheme';

import './Demo.scss';

export default () => {
    const blocks = [
            {id: 1, title: 'Запрос', position: [210, 10], rows: [
                {text: 'нет ошибок', relatable: true, relations: []},                
                {text: 'Ошибка', relatable: true, relations: [2]},                
            ], relations: [2]},
            {id: 2, title: 'Ответ с ошибкой', position: [200, 150], rows: [
                {text: 'auth_required', relatable: true, relations: [3]},
                {text: 'access_token_not_exists', relatable: true, relations: [4]},
                {text: 'token_required', relatable: true, relations: [9]},
                {text: 'refresh_not_exists', relatable: true, relations: [12]},
                
                {text: 'token_banned', relatable: true, relations: []},
                {text: 'permission_denied', relatable: true, relations: []},
            ]},         
            {id: 3, title: 'Авторизация', position: [430, 100], rows: [
                {text: 'Yes', relatable: true, relations: [7]},
                {text: 'No', relatable: true, relations: [8]},
            ]},
            {id: 4, title: 'refresh', position: [820, 180], rows: [
                {text: 'нет ошибок', relatable: true, relations: [7]},
            ]},
            // {id: 5, title: 'set_access', position: [430, 200], rows: [
            //     {text: '-->', relatable: true, relations: []},
            // ]},
            // {id: 6, title: 'new', position: [430, 270], rows: [
            //     {text: '-->', relatable: true, relations: []},
            // ]},  
            {id: 7, title: 'resend', position: [600, 60], rows: [
                {text: '-->', relatable: true, relations: [13]},
            ]},   
            {id: 8, title: 'Ждем авторизации', position: [630, 140], rows: []},   
            {id: 9, title: 'Есть в хранилище AccessToken', position: [450, 210], rows: [
                {text: 'Yes', relatable: true, relations: [10]},
                {text: 'No', relatable: true, relations: [11]},                
            ]},
            {id: 10, title: 'set_access', position: [820, 250], rows: [
                {text: 'нет ошибок', relatable: true, relations: [7]},
            ]},      
            {id: 11, title: 'new', position: [820, 320], rows: [
                {text: 'нет ошибок', relatable: true, relations: [7]},
            ]},    
            {id: 12, title: 'Очистить локально токены', position: [450, 320], rows: [
                {text: '-->', relatable: true, relations: [7]},
            ]},   
            {id: 13, title: 'нет в списке', position: [430, 10], rows: [
                {text: ['set_access', 'refresh', 'new'].join('\n'), relatable: true, relations: [1]},
            ]},                                                                              
        ],
        // [selected, setSelected] = useState(null),
        onSelect = (a,b,c) => console.log('onSelect', a,b,c),
        onBlockMove = (a,b,c) => console.log('onSelect', a,b,c),
        onBlockLinkAdd = (a,b,c) => console.log('onSelect', a,b,c),
        onBlockLinkRemove = (a,b,c) => console.log('onSelect', a,b,c),
        getInfoFragment = (a,b) => console.log(a,b);



    return (<div className='demo'>
    <div className='scheme-demo-canvas'>
        
        <Scheme
            onSelect={onSelect}
            onMoveEnd={onBlockMove}
            onLinkAdd={onBlockLinkAdd}
            onLinkRemove={onBlockLinkRemove}
            // ref={ref => this.$scheme = ref}
        >
            {(blocks || []).map(block => (<SchemeBlock
                key={block.id}
                id={block.id}
                title={block.title}
                position={block.position}
                relations={block.relations}
                styles={block.styles}
            >
                {block.rows?.map((row, ix) => <SchemeBlockRow
                    key={ix}
                    content={row.text}
                    relatable={row.relatable}
                    relations={row.relations}
                    onClick={row.onClick}
                />)}
                {block.info?.map((infoRow, ix) => {
                    if(infoRow.length === 1) {
                        return (<SchemeBlockRow
                            key={ix}
                            content={opts => getInfoFragment(opts, infoRow[0])}
                        />);
                    } else {
                        return (<SchemeBlockRow key={ix}>{infoRow.map((f, fIX) => (<RowFragment
                            onClick={f.type === 'link' && (() => onLink(f.href))}
                            // onMouseOver={() => console.log('onMouseOver')}
                            // onMouseLeave={() => console.log('onMouseLeave')}
                            key={fIX}
                            content={opts => getInfoFragment(opts, f)}
                        />))}</SchemeBlockRow>);
                    }
                })}
            </SchemeBlock>))}
        </Scheme>
    </div>
</div>)
}