import { useCallback, useRef, useState } from 'react';
import isFoo from 'reneco-utils/isFoo';

export default onChange => {
    const rows = useRef([]),
        [rowsMeta, setRowsMeta] = useState([]),
        to = useRef(null),
        getRowByIX = useCallback(ix => rows.current.find(([six]) => six === ix), []),
        setData = () => onChange 
            ? onChange(rows.current.slice())
            : setRowsMeta(rows.current.slice()),
        syncToState = useCallback(() => {
            if(to.current) {
                clearTimeout(to.current)
            }
            to.current = setTimeout(setData);
        }, []),
        register = useCallback(rowData => {
            rows.current.push(isFoo(rowData) ? rowData(rows.current) : rowData);
            syncToState();
            return rows.current.length-1;
        }, []),
        unregister = useCallback(ix => {
            const fix = rows.current.findIndex(([six]) => six === ix);
            if(fix > -1) {
                rows.current.splice(fix, 1);
                syncToState();
            }
        }, []),
        update = useCallback((ix, data, immediate) => {
            const fix = rows.current.findIndex(([six]) => six === ix);
            if(fix > -1) {
                rows.current.splice(fix, 1, (data => [ix, ...data])(isFoo(data) ? data(rows.current[fix].slice(1)) : data));
                !immediate ? syncToState() : setData();
            }            
        }, []);
    return [rowsMeta, register, unregister, update, () => rows.current || [], getRowByIX];
}