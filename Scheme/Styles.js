import defaultStyle from './defaultStyle';
import withEvents from 'reneco-utils/withEvents';

const STYLE_EVENTS = {
    change: 'STYLE_CHANGED'
};

export default withEvents(class Styles {
    constructor(styleList) {
        this.styles = Object.assign({}, defaultStyle, styleList || {});
        this._cache = {};
    }
    getStyleValue(style, delimiter = '.') {
        if(this.styles[style] !== undefined) {
            return this.styles[style];
        } else {
            const splitted = style.split(delimiter).reverse();
            for(let i=0; i < splitted.length; i++) {
                const ka = splitted.slice(1+i),
                    k = ka.reverse().concat([splitted[0]]).join(delimiter);
                if(this.styles[k] !== undefined) {
                    return this.styles[k];
                } else { /// А если присутствует hover
                    let iix;
                    const ka2 = ka.slice();
                    while((iix = ka2.findIndex(kk => ['hover', 'selected'].indexOf(kk) > -1)) > -1) {
                        ka2.splice(iix, 1);
                        const k2 = ka2.concat([splitted[0]]).join(delimiter);
                        if(this.styles[k2] !== undefined) {
                            return this.styles[k2];
                        }
                    }
                }
            }
            return false;
        }
    }
    getStyle(style) {
        if(this._cache[style] === undefined) {
            this._cache[style] = this.getStyleValue(style);
        }
        // console.log(style, this._cache[style])
        return this._cache[style];
    }
    update(styles, silent) {
        this.styles = Object.assign({}, this.styles, styles);
        this._cache = {};
        !silent && this.dispatchEvent(EVENTS.change, this);
    }
    destruct() {
        this.removeEventListener(EVENTS.change);
        this._cache = null;
        this.styles = null;
    }
});

export const EVENTS = STYLE_EVENTS;