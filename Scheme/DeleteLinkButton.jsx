import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import IconDelete from './icons/close.svg';
import getMiddlePoint from 'reneco-utils/Curves/getMiddlePoint';
import useLink from './useLink';

export default ({
    onDelete, 
    size: buttonSize = 15,
    bg = '#fff',
    width,
    border,
    x1,
    x2,
    y1,
    y2,
    targetPosition
}) => {
    const baseClass = 'reneco-components-scheme-link',
        $handler = useRef(null),
        [c1x, c1y, c2x, c2y] = useLink({x1, x2, y1, y2, targetPosition}),
        [x, y] = useMemo(() => getMiddlePoint([
            [x1,y1],
            [c1x,c1y],
            [c2x,c2y],
            [x2,y2],
        ]), [x1, x2, y1, y2, targetPosition]),
        onMouseDown = useCallback(e => {
            e.stopPropagation();
            onDelete && onDelete(e);
        });   
    useEffect(() => (eMouseDown => {
        $handler.current.addEventListener(eMouseDown, onMouseDown)
        return () => {
            $handler.current && $handler.current.removeEventListener(eMouseDown, onMouseDown)
        }
    })('mousedown'), []);
    return (onDelete && <>
        <circle
            cx={x}
            cy={y}
            r={buttonSize}
            fill={bg}
            stroke={border}
            strokeWidth={width}
        />
        {<image
            x={x - buttonSize/2}
            y={y - buttonSize/2}
            xlinkHref={IconDelete}
            width={buttonSize}
            height={buttonSize}
        />}

        <circle
            ref={$handler}
            className={`${baseClass}-handle`}
            cx={x}
            cy={y}
            r={buttonSize + buttonSize*0.3}
            // onClick={onDelete}
        />
    </>);
}