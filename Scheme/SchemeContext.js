import React from 'react';

const Context = React.createContext(),
    { Provider, Consumer} = Context;
export {Context, Provider, Consumer};