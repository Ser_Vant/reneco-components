import React from 'react';

export default ({
    dropShadow,
    id
}) => {
    const ds = dropShadow && dropShadow.split(' ') || [];
    return (<filter
        id={id}
        height={3}
        width={3}
        x={-1}
        y={-1}
    >
        <feDropShadow
            dx={ds[0].replace('px', '')}
            dy={ds[1].replace('px', '')}
            stdDeviation={ds[2].replace('px', '')}
            floodColor={ds[3]}
            // floodOpacity={0.4}
        />
    </filter>);
}