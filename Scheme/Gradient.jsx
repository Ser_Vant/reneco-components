import React from 'react';

export default ({
    colors = [],
    id = '',
    x1 = 0,
    x2 = 0,
    y1 = 0,
    y2 = 1
}) => (<linearGradient
    x1={x1}
    y1={y1}
    x2={x2}
    y2={y2}
    id={id}
>
    {colors.map((color, ix) => (<stop
        key={ix}
        stopColor={color}
        offset={`${ix/(colors.length-1) * 100}%`}
    />))}
</linearGradient>);