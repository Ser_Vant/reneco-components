const PnpWebpackPlugin = require('pnp-webpack-plugin'),
    MiniCssExtractPlugin = require('mini-css-extract-plugin'),
    path = require('path'),
    isProduction = process.env.NODE_ENV === 'production',
    cssLoaders = env => ([
        {
            loader: MiniCssExtractPlugin.loader,
            options: {
                esModule: true,
            },
        },
        {
            loader: 'css-loader',
            options: {
                sourceMap: (env && env.development),
                importLoaders: 1
            }
        },
        {
            loader: 'postcss-loader',
            options: {
                sourceMap: (env && env.development),
                postcssOptions: {
                    ident: 'postcss',
                    plugins: [
                        require.resolve('autoprefixer'),
                    ]
                }
            }
        }
    ]);
module.exports = env => ({
    mode: (!env || !env.production)
        ? 'development'
        : 'production',
    devServer: {
        // contentBase: path.join(__dirname, ''),
        // historyApiFallback: true,
        // compress: true,
        // disableHostCheck: true,
        static: {
            directory: path.join(__dirname, ''),
        },
        historyApiFallback: true,
        compress: true,
        // // disableHostCheck: true,
        port: 5066       
    },
    entry: {
        bundle: path.resolve('./example.jsx')
    },
    watch: !env || !env.production,
    devtool: (!env || !env.production)
        && 'eval'
        // && 'cheap-module-source-map'
        || 'source-map',
    output: {
        filename: '[name].js',
        path: path.resolve('./tmp'),
        publicPath: '/tmp/'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: [{
                    loader: require.resolve('babel-loader'),
                    options: {
                        presets: [
                            require.resolve('@babel/preset-react'),
                            require.resolve('@babel/preset-env'),
                        ],
                        plugins: [
                            [
                                require.resolve('babel-plugin-import'),
                                {
                                    libraryName: 'antd',
                                    style: true
                                }
                            ],
                            // [
                            //     require.resolve('@babel/plugin-proposal-decorators'),
                            //     {
                            //         legacy: true
                            //     }
                            // ],
                            // [
                            //     require.resolve('@babel/plugin-proposal-class-properties'),
                            //     {
                            //         loose: true
                            //     }
                            // ]
                        ]
                    },
                }],
            },
            {
                test: /\.(css)$/,
                use: cssLoaders(env).slice()
            },
            {
                test: /\.(sass|scss)$/,
                use: cssLoaders(env).slice().concat([
                    {
                        loader: require.resolve('sass-loader'),
                        options: {
                            sourceMap: !isProduction,
                        },
                    },
                ])
            },
            {
                test: /\.(less)$/,
                use: cssLoaders(env).slice().concat([
                    {
                        loader: require.resolve('less-loader'),
                        options: {
                            lessOptions: {
                                javascriptEnabled: true,
                            }
                        }
                    }
                ])
            },
            {
                test: /\.(png|gif|jpg|svg|ttf|eot|woff|woff2)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]'
                    }
                }
            },
            {
                test: /node_modules[/\\]trix.*\.js/,
                use: [
                    {
                        loader: 'imports-loader',
                        options: {
                            this: '>window'
                        }
                    }
                ]
            }
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // all options are optional
            filename: '[name].css',
            chunkFilename: '[id].css',
            // ignoreOrder: false, // Enable to remove warnings about conflicting order
        }),
    ],
    resolveLoader: {
        plugins: [
            PnpWebpackPlugin.moduleLoader(module),
        ],
    },
    resolve: {
        plugins: [
            PnpWebpackPlugin,
        ],
        extensions: ['.json', '.js', '.jsx'],
        modules: [
            'node_modules'
        ],
    }
});
