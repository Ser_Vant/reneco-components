import './Loader.sass';
import Spinner from 'react-spinkit';
import React from 'react';

export default function Loader(props) {
    return (<div className='loader'>
        <Spinner name='three-bounce' fadeIn='none' />
    </div>);
}
