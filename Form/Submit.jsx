import React from 'react';
import FormButton from './Button';

export default function FormSubmit(props) {
    return (<FormButton {...props} />);
}
