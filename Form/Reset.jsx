import React from 'react';
import FormButton from './Button';

export default function FormReset(props) {
    return (<FormButton  {...props} />);
}
