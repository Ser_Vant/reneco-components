import React from 'react';

import addClass from 'reneco-utils/addClass';

export default function FormButton(props) {
    let baseClass = 'form-button';
    return (<button
        onClick={props.onClick}
        className={addClass(baseClass, props.className)}
    >{props.children}</button>);
}
