import React from 'react';
import CommonRow from './Common';

export default class FormInput extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            value: props.value
        };
    }
    componentWillReceiveProps(nProps) {
        let state = this.state,
            nState = {};
        if(nProps.value !== state.value) {
            nState.value = nProps.value;
        }
        if(Object.keys(nState).length>0) {
            this.setState(nState);
        }
    }
    onChange = e => {
        let value = e.target.value;
        if (this.props.mask) {
            console.info('Not realized!!');
        }
        this.setState({value: value});
        this.props.onChange && this.props.onChange(this.props.name, value);
    }
    render() {
        let props = this.props,
            state = this.state,
            type = props.type || 'text';
        return (<CommonRow error={props.error} label={props.label}>
            <input name={props.name} type={type} value={state.value} onChange={this.onChange} />
        </CommonRow>);
    }
}
