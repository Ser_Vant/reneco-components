import React from 'react';

import FormInput from './Input';
import FormReset from './Reset';
import FormButton from './Button';
import FormSubmit from './Submit';

import unicalizeComponent from 'reneco-utils/unicalizeComponent';

unicalizeComponent([
    FormInput, FormReset, FormButton, FormSubmit
]);

const getComponentName = item=>{return item.name; },
    FormComponents = [
        FormInput,
        FormReset,
        FormButton,
        FormSubmit
    ],
    InputComponents = [ // form field data
        FormInput
    ],
    ButtonComponents = [
        FormReset,
        FormButton,
        FormSubmit
    ];


export default class Form extends React.Component {
    fields = {}
    state = {
        fields: {},
        errors: {}
    }
    values = {}
    initialValues = {}
    renderForm (child) {
        return React.Children.map(child, ch => {
            let ret;
            if(ch) {
                if (FormComponents.findIndex(c => c.isInstance(ch)) > -1) {
                    let opts;
                    if(InputComponents.findIndex(c => c.isInstance(ch)) > -1) { // is field
                        opts = {
                            ref: ref=> {
                                this.fields[ch.props.name] = ref;
                            },
                            value: this.state.fields[ch.props.name] !== undefined
                                ? this.state.fields[ch.props.name]
                                : ch.props.value || '',
                            onChange: this.onFieldChange
                        };
                        this.values[ch.props.name] = opts.value;
                    } else if(ButtonComponents.findIndex(c => c.isInstance(ch)) > -1) {
                        opts = {
                            onClick: FormSubmit.isInstance(ch)
                                ? this.onClickSubmit
                                : (FormReset.isInstance(ch)
                                    ? this.onClickReset
                                    : null
                                )
                        };
                    }
                    ret = React.cloneElement(ch, opts);
                } else if(ch.props && ch.props.children){
                    ret = this.renderForm(ch.props.children);
                } else {
                    ret = ch;
                }
            }
            return ret;
        }).reduce((prev, cur) => {
            cur && prev.push(cur);
            return prev;
        }, []);
    }
    onFieldChange = (name, value)=> {
        this.values[name] = value;
    }
    onClickSubmit = e=> {
        e.preventDefault();
        console.log('Submit', this.values);
        this.props.onSubmit && this.props.onSubmit(this.values);
    }
    onClickReset = e=> {
        e.preventDefault();
        console.log('reset', this.initialValues);
        this.props.onReset && this.props.onReset();
    }
    componentDidMount() {
        let nState = {};
        Object.keys(this.fields).forEach(fName=>{
            let fComponent = this.fields[fName];
            nState[fName] = fComponent.props.value;
        });
        // this.setState(nState);
        this.values = this.initialValues = nState;
        // console.log(nState);
    }
    render() {
        let state = this.state,
            props = this.props,
            baseClass = 'form';
        return (<form className={baseClass}>
            {this.renderForm(props.children)}
        </form>);
    }
}
