import React from 'react';

export default function FormRow(props) {
    let baseClass = 'form-row';
    return (<div className={baseClass}>

        {props.children }
    </div>);
}
