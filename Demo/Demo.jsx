import React from 'react';
import Menu from '../Menu';
import Button from '../Form/Button';
import {createBrowserHistory} from 'history';


export const Route = () => (<React.Fragment />);

export default ({children}) => {

    const baseClass = 'demo-app';
    return (<div className={baseClass}>
        {children}
    </div>)
}