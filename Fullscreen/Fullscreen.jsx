import React from 'react';
import addClass from 'reneco-utils/addClass';
import Fullscreen from 'reneco-utils/Fullscreen';

export default class FullscreenComponent extends React.Component {
    static defaultProps = {
        list: [],
        autoplay: 3,
        showSlider: true
    }
    constructor(props) {
        super(props);
        this.Fullscreen = new Fullscreen();
        this.state = {
            fullscreen: props.fullscreen === true
        };
    }
    onFullScreenToggle = e => {
        if (this.state.fullscreen) {
            this.Fullscreen.exitFullscreen();
        } else {
            this.Fullscreen.fullscreen();
        }
    }
    onFullScreenChange = isFullscreen => {
        if (this.props.onExit && this.state.fullscreen !== isFullscreen && !isFullscreen) {
            this.props.onExit();
        }
        this.setState({
            fullscreen: isFullscreen
        });
    }
    componentDidMount() {
        this.Fullscreen.onChange(this.Box, this.onFullScreenChange);
        if (Fullscreen.isEnabled() && this.props.fullscreen) {
            this.Fullscreen.fullscreen();
        }
    }
    fullscreen() {
        this.Fullscreen.fullscreen();
    }
    exitFullscreen() {
        this.Fullscreen.exitFullscreen();
    }
    componentWillUnmount() {
        this.Fullscreen.destroy();
    }
    render() {
        let props = this.props,
            state = this.state,
            baseClass = 'fullscreen';
        return (<div
            ref={ref => { this.Box = ref; }}
            className={addClass(baseClass, props.className)}
            style={props.style || {}}
        >
            {props.children}
        </div>);
    }
}
