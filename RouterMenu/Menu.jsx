import React from 'react';
import PropTypes from 'prop-types';
import Menu from '../Menu';

import './Menu.scss';

export default class RouterMenu extends React.Component{
    static contextTypes = {
        route: PropTypes.object.isRequired,
        getRootRoutePath: PropTypes.func.isRequired
    }
    render() {
        let rootPath = this.context.getRootRoutePath().replace(/\/\//g, '/'),
            menuList = this.context.route.childRoutes.map((r, ix)=>{
                return {
                    caption: r.caption || r.path,
                    to: ((rootPath?('/'+rootPath):'')+'/'+r.path).replace(/\/\//g, '/')
                };
            });
        return (<Menu itemTag={this.props.itemTag} list={menuList} className='router-menu' />);
    }
}
