import React from 'react';

export default function CustomTag(props) {
    const tagList = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'span', 'ul', 'ol', 'li'],
        tagName = props.tag
            || tagList.find(tag => props[tag])
            || 'div';
    return React.createElement(
        tagName,
        Object.keys(props || {}).reduce((prev, cur) => {
            if(tagList.indexOf(cur) < 0) {
                prev[cur] = props[cur];
            }
            return prev;
        }, {}),
        props.children
    );
}