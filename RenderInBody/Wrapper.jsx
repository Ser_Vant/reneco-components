import React from 'react';
import PropTypes from 'prop-types';

export default class Wrapper extends React.Component {
    static childContextTypes = {
        router: PropTypes.object,
        location: PropTypes.object,
        body: PropTypes.object,
        getHeader: PropTypes.func,
        showPopup: PropTypes.func,
        hidePopup: PropTypes.func
    };
    getChildContext() {
        let props = this.props;
        return {
            router: props.router,
            location: props.location,
            body: props.body,
            getHeader: props.getHeader,
            showPopup: props.showPopup,
            hidePopup: props.hidePopup
        };
    }
    render() {
        return (<div>{this.props.children}</div>);
    }
}
