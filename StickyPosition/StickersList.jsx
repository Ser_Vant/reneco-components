import sumBy from 'lodash/sumBy';
let counter = 0;
class Stickers {
    list = []
    add(item){
        item._ix = counter++;
        this.list.push(item);
    }
    remove(item){
        let ix = this.list.findIndex(listItem=>{
            return item._ix === listItem._ix;
        });
        if(ix>-1){
            this.list.splice(ix, 1);
        }
    }
    getHeight(item){
        let list = item
            ?this.list.slice(0, this.list.findIndex(listItem=>{
                return item._ix === listItem._ix;
            }))
            :this.list,
            ret =  sumBy(list, item=>{
                return item.Sticker && item.Sticker.getHeight() || 0;
            });
        return ret;
    }
    heightChanged(item) {
        let list = item
            ?this.list.slice(this.list.findIndex(listItem=>{
                return item._ix === listItem._ix;
            }))
            :this.list;
        list.forEach(sp=>{
            sp.onScroll();
        });
    }
}

export default new Stickers();
