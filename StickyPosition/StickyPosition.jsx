import React from 'react';
import ReactDOM from 'react-dom';
import getDocumentScroll from 'reneco-utils/getDocumentScroll';
import getOffsetRect from 'reneco-utils/getOffsetRect';
import isSupportSticky from 'reneco-utils/isSupportSticky';
import addClass from 'reneco-utils/addClass';

import PositionStickySupported from './PositionStickySupported';
import PositionStickyNotSupported from './PositionStickyNotSupported';
import Stickers from './StickersList';
import Clear from '../Clear';

const throttle = (callback, to) => {
    let _to = null;
    return () => {
        if(!_to) {
            _to = setTimeout(() => {}, to);
            callback();
        }
    }
}

export default class StickyPosition extends React.Component{
    state = {
        headerHeight: 0,
        initialHeight: 0,
        placeholderHeight: 0,
        isFixed: undefined
    }
    componentDidMount(){
        // this.setState({
        //     initialHeight: ReactDOM.findDOMNode(this.Sticker).offsetHeight
        // });
        Stickers.add(this);
        this.onScroll = throttle(this.onScroll, 50);
        this.box = ReactDOM.findDOMNode(this);
        window.addEventListener('scroll', this.onScroll);
        this.onAnimationEnd = throttle(this.onAnimationEnd, 50);
        this.onHeightChange = throttle(this.onHeightChange, 50);
        this.onScroll();
    }
    onAnimationEnd = ()=>{
        const scComp = this.Sticker;
        scComp.calculateHeight && scComp.calculateHeight();
        this.onHeightChange();
    }
    componentWillUnmount(){
        Stickers.remove(this);
        window.removeEventListener('scroll', this.onScroll);
    }
    onScroll = ()=>{
        const newState = {},
            headerHeight = this.getHeaderHeight(),
            sticker = this.Sticker;

        if(headerHeight !== this.state.headerHeight) {
            newState.headerHeight = headerHeight;
        }


        sticker && sticker.onScroll && sticker.onScroll();

        let scroll = getDocumentScroll().y,
            minY = getOffsetRect(this.box).y - headerHeight,
            maxY = minY+this.box.offsetHeight-(sticker && sticker.getHeight() || 0),
            fixed = false;
        if(this.props.fixed || (scroll>=minY && scroll <= maxY)) {
            fixed = true;
        }
        newState.isFixed = fixed;
        if(fixed !== this.state.isFixed) {
            sticker && sticker.fixedUpdate(fixed);
        }

        if(newState.isFixed !== this.state.isFixed || newState.headerHeight !== this.state.headerHeight) {
            this.setState(newState);
        }
    }
    // componentDidUpdate(){
    //     let state=this.state,
    //         newPlaceHolderHeight = Utils.isSupportSticky()
    //             ? state.initialHeight-ReactDOM.findDOMNode(this.Sticker).offsetHeight
    //             : state.initialHeight;
    //     if(newPlaceHolderHeight !== state.placeholderHeight) {
    //         this.setState({
    //             placeholderHeight: newPlaceHolderHeight
    //         });
    //     }
    // }
    getHeaderHeight(){
        return Stickers.getHeight(this);
    }
    getStickyBox(){
        return this.Sticker && this.Sticker.getComponent();
    }
    isFixed(){ return this.state.isFixed; }
    onHeightChange = newHeight => {
        Stickers.heightChanged(this);
    }
    render() {
        const {
                stickyZIndex,
                stickyHeader,
                fixed,
                className,
                contentClassName,
                children
            } = this.props,
            {
                isFixed,
                headerHeight
            } = this.state,
            isSticky = isSupportSticky(),
            StickyComponent = stickyHeader || <div />,
            Sticker = isSticky && !fixed && PositionStickySupported || PositionStickyNotSupported,
            baseClass = 'c-sticky-position';
        let _className = '';
        if(stickyZIndex){
            _className = `z-index-${stickyZIndex}`;
        }
        // placeholderHeight = state.initialHeight;
        return (<div className={addClass(className, baseClass)}>
            <Sticker
                className={_className}
                onAnimationEnd={this.onAnimationEnd}
                ref={ref=>{this.Sticker = ref;}}
                isFixed={isFixed}
                headerHeight={headerHeight}
                component={StickyComponent}
                onHeightChange={this.onHeightChange}
            />
            {/*<div className={baseClass + '__placeholder'} style={{ height: state.placeholderHeight }} />*/}
            <div className={addClass('sticky-content', contentClassName)}>{children}</div>
            <Clear />
        </div>);
    }
}
