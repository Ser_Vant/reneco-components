import React from 'react';
import ReactDOM from 'react-dom';
import Sticky from './sticky.jsx';
import addClass from 'reneco-utils/addClass';
import size from 'lodash/size';
import extend from 'lodash/extend';
import clone from 'lodash/clone';

export default class PositionStickyNotSupported extends Sticky{
    state = {
        height:0,
        maxHeight:0
    }
    shouldComponentUpdate(nProps, nState){
        return super.shouldComponentUpdate(nProps)
            || nState.height !== this.state.height
            || nState.maxHeight !== this.state.maxHeight
            || nProps.isFixed !== this.props.isFixed;
    }
    calculateHeight(){
        let h = this.Sticky && ReactDOM.findDOMNode(this.Sticky).offsetHeight || 0,
            nState = {};

        if(h !== this.state.height) {
            nState.height = h;
            this.heightChanged(h);
        }
        if(h > this.state.maxHeight) {
            nState.maxHeight = h;
        }
        if(size(nState)>0){
            this.setState(nState);
        }
    }
    getHeight(){
        return this.state.height;
    }
    render(){
        let props = this.props,
            InnerComponent = this.props.component,
            style = {},
            placeholderStyle = {},
            className = 'position-static';
        if(props.isFixed) {
            style.top = this.props.headerHeight;
            className = 'position-fixed';
            placeholderStyle.height = this.state.maxHeight;
        }
        return (<div>
            <div style={style} className={addClass(className, props.className)}>
                {InnerComponent}
            </div>
            <div style={placeholderStyle} className='placeholder' />
        </div>);
    }
}
