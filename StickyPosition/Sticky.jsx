import React from 'react';
import ReactDOM from 'react-dom';
import isEqual from 'lodash/isEqual';


export default class StickyPosition extends React.Component{
    sticky = null
    shouldComponentUpdate(nProps){
        return nProps.headerHeight !== this.props.headerHeight
            || !isEqual(nProps.stickyProps, this.props.stickyProps);
    }
    fixedUpdate(fixed){
        this.Sticky && this.Sticky.onFixedChange && this.Sticky.onFixedChange(fixed);
    }
    getComponent(){
        return this.Sticky;
    }
    getHeight(){
        return (this.Sticky && this.Sticky.getHeight && this.Sticky.getHeight()
            || this.Sticky && ReactDOM.findDOMNode(this.Sticky).offsetHeight
            || 0);
    }
    calculateHeight(){}
    componentDidMount(){
        this.calculateHeight();
    }
    componentDidUpdate(){
        this.calculateHeight();
    }
    resolveRef = ref=>{
        if(this.props && this.props.stickyProps._ref) {
            this.props.stickyProps._ref(ref);
        }
        this.Sticky = ref;
    }
    heightChanged = newHeight =>{
        this.props.onHeightChange && this.props.onHeightChange(newHeight);
    }
}
