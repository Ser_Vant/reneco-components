import React from 'react';
import Sticky from './sticky.jsx';
import addClass from 'reneco-utils/addClass';

export default class PositionStickySupported extends Sticky{
    render(){
        let InnerComponent = this.props.component,
            stickyProps = Object.assign({
                onAnimationEnd: this.props.onAnimationEnd,
                ref: this.resolveRef
            }, this.props.stickyProps||{}),
            placeholderStyle = {height: 30};
        return (<div style={{ top: this.props.headerHeight }} className={addClass('position-sticky', this.props.className)} >
            <div >
                <InnerComponent {...stickyProps} />
            </div>
            {/*<div style={placeholderStyle} className='placeholder' />*/}
        </div>);
    }
}
