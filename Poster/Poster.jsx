import React from 'react';
import addClass from 'reneco-utils/addClass';
import Posterable from './Posterable';

import './Poster.sass';

export default Posterable(({
    caption,
    height,
    width,
    className,
    alt,
    onClick,
    description1,
    description2,
    loaded,
    src
}) => {
    const baseClass = 'poster',
        imgHeight = (height || 200) / (width || 300) * 100;

    return (<div className={addClass(baseClass, className)}>
        {loaded
            && <img src={src} alt={alt} onClick={onClick} />
            || <div
                style={{paddingBottom: `${imgHeight}%`}}
                className={`${baseClass}__loader`}
            >
                <div className={`${baseClass}__loader--inner`} />
            </div>}
        {caption && <b>{caption}</b>}
        {(description1 || description2) && <div className={`${baseClass}__descriptions`}>
            {description1 && <div className={`${baseClass}__description-1`}>{description1}</div>}
            {description2 && <div className={`${baseClass}__description-2`}>{description2}</div>}
        </div>}
    </div>);
});
